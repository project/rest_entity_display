# Rest Entity Display

**This module leverages the power of rest resources, formatters and serializers to expose entities through view modes and form modes. In fine, it exposes EntityViewDisplay and EntityFormDisplay core config entities, which are a combination of an entity and a view or form mode.**

- Relies on core view and form modes and formatters to handle everything in config.
- Relies on core permissions.
- Does not output HTML in formatted output but normalized arrays of values as JSON.
- Allows custom field names for API, per view mode, through normalized formatters settings.
- Handles deep entities through a normalized embed entity reference formatter.
- Can be easily extended with your own resources to have your own API path.

## Dependencies

Rest Entity Display only relies on core Rest module.

## Configuration

By default, nothing is enabled. You need some simple steps to configure your resources:

1. Enable the "Entity view display" resource and give some permissions to user roles of your choice for this resource.
2. Create use an existing view mode for the entity types you want to expose (like a view mode named "API" for example).
3. Enable some fields inside your view mode and choose normalized formatters for display (see below to create normalized formatters for non-core fields).
4. Navigate to `/admin/config/services/rest-entity-display` (Configuration > Web Services > Rest Entity Display settings). Here, activate the entity types of your choice and the view modes that will be exposed (like the "API" one you may have created).
6. Your resource is now enable here: `/api/rest_entity_display/{entity_type_id}/{entity_id}/{view_mode}`

## Use case example

After enabling the module and the resource and its permissions:

1. Create a few API view mode ("API full" / "API medium" / "API short") for node and activate them in the node _Manage Display_ tab.
2. Enable them in the rest entity display config page for your entity.
3. Install Base Field Display module.
4. Populate your API view modes with fields and base fields and use the normalized formatters on them.
5. Configure the normalized embed entity reference formatters to use "api_short" view mode.
6. Access your endpoint here for full display of node ID 1: `/api/rest_entity_display/node/1/api_full`

## Extend

### Custom name for exposed fields

You can set any name you want inside each exposed view mode, when you choose a normalized formatter. A setting form is associated with it, allowing you to chose, for example, "customName" to replace default "field_custom_name".

### Custom resource path

You need to create another rest resource Plugin inside your module. You can extends `src/Plugin/rest/resource/EntityViewDisplayResource.php` class and use your own path in the Resource annotations.

### Custom formatter

This module contains normalized formatters for core fields. If you need some custom or contrib modules to have a normalized formatters for the fields they provide, you need to create it in your own module or to create an issue in the contrib module page. You can find some examples inside Formatter folder : `src/Plugin/Field/FieldFormatter`

### Expose entity properties (base fields)

Use the BaseField Display module to expose entity's properties (title, uuid, author...) in your view modes. Rest Entity Display module handle some normalized formatters for them.
