<?php

namespace Drupal\rest_entity_display\Exception;

/**
 * Defines an exception thrown when entity is not fieldable.
 */
class EntityNotFieldableException extends \Exception {}
