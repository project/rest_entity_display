<?php

namespace Drupal\rest_entity_display\Exception;

/**
 * Defines an exception thrown when entity display is not activated.
 */
class EntityDisplayNotActivatedException extends \Exception {}
