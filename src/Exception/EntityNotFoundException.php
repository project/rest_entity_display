<?php

namespace Drupal\rest_entity_display\Exception;

/**
 * Defines an exception thrown when entity was not found.
 */
class EntityNotFoundException extends \Exception {}
