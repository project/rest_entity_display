<?php

namespace Drupal\rest_entity_display;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Entity display manager class.
 */
interface EntityDisplayManagerInterface {

  /**
   * Check if an entity type is activated in config and can expose a display mode.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $mode
   *   The view mode.
   * @param string $context
   *   Either 'view' or 'form'.
   *
   * @return bool
   *   TRUE if entity type is activated.
   */
  public function isEntityDisplayActivated(string $entity_type_id, string $mode, string $context): bool;

  /**
   * Gets a normalized entity view/form display config entity from a fieldable
   * entity and a view/form mode.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $entity_id
   *   The entity id.
   * @param string $mode
   *   The view or form mode.
   * @param string $context
   *   Either 'view' or 'form'.
   *
   * @return array
   *   The normalized EntityViewDisplay or EntityFormDisplay config entity.
   */
  public function getNormalizedEntityDisplay(string $entity_type_id, string $entity_id, string $mode, string $context): array;

  /**
   * Loads a fieldable entity.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $id
   *   The entity identifier.
   *
   * @return FieldableEntityInterface
   */
  public function getFieldableEntity(string $entity_type_id, string $id): FieldableEntityInterface;

  /**
   * List view and form mode with their entity display options.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   The options keyed by mode (view or form).
   */
  public function getModeOptions(string $entity_type_id): array;

}
