<?php

namespace Drupal\rest_entity_display\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rest_entity_display\EntityDisplayManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Rest Entity Display configuration form.
 */
class RestEntityDisplaySettingsForm extends ConfigFormBase {

  /**
   * The settings name.
   *
   * @var string
   */
  const SETTINGS = 'rest_entity_display.settings';

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  protected EntityDisplayManagerInterface $entityDisplayManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    /** @var EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->setEntityTypeManager($entity_type_manager);

    /** @var EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = $container->get('entity_field.manager');
    $instance->setEntityFieldManager($entity_field_manager);

    /** @var EntityDisplayManagerInterface $entityDisplayManager */
    $entity_display_manager = $container->get('rest_entity_display.entity_display_manager');
    $instance->setEntityDisplayManager($entity_display_manager);

    return $instance;
  }

  /**
   * Sets the entity type manager.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the entity field manager.
   *
   * @param EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function setEntityFieldManager(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Sets the entity display manager.
   *
   * @param EntityDisplayManagerInterface $entity_display_manager
   *   The entity display manager.
   */
  public function setEntityDisplayManager(EntityDisplayManagerInterface $entity_display_manager) {
    $this->entityDisplayManager = $entity_display_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rest_entity_display_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $resource_id = NULL): array {
    $config = $this->config(static::SETTINGS);
    $form['#title'] = $this->t('REST Entity Display settings');
    $form['#tree'] = TRUE;

    $entity_types = $this->entityTypeManager->getDefinitions();
    usort($entity_types, function ($a, $b) {
      return strcmp($a->getLabel(), $b->getLabel());
    });

    $form['entities_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    foreach ($entity_types as $entity_type) {
      if (
        $entity_type->hasViewBuilderClass() &&
        $entity_type->entityClassImplements(FieldableEntityInterface::class)
      ) {

        $form['entities'][$entity_type->id()] = [
          '#type' => 'details',
          '#title' => $entity_type->getLabel(),
          '#group' => 'entities_tabs',
        ];

        foreach ($this->entityDisplayManager->getModeOptions($entity_type->id()) as $mode => $mode_options) {
          $form['entities'][$entity_type->id()][$mode] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Expose %entity_type entity type in Entity ' . $mode . ' display resource', ['%entity_type' => $entity_type->id()]),
            '#default_value' => $config->get('entities.' . $entity_type->id() . '.' . $mode) ?? 0,
            '#attributes' => [
              'data-activated' => 'entities_' . $entity_type->id() . '_' . $mode,
            ],
          ];
          $form['entities'][$entity_type->id()][$mode . '_modes'] = [
            '#type' => 'checkboxes',
            '#title' => $this->t('Modes to expose in Entity ' . $mode . ' display resources'),
            '#description' => $this->t('If none are checked, all ' . $mode . ' modes will be exposed.'),
            '#options' => $mode_options,
            '#default_value' => $config->get('entities.' . $entity_type->id() . '.' . $mode . '_modes') ?? [],
            '#states' => [
              'enabled' => [
                ':input[name="entities[' . $entity_type->id() . '][' . $mode . ']"]' => ['checked' => TRUE],
              ],
            ],
          ];
        }

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_values = [];
    foreach ($form_state->getValue('entities') as $entity_type_id => $entity_type_config) {

      foreach ($this->entityDisplayManager->getModeOptions($entity_type_id) as $mode => $mode_options) {
        if ($entity_type_config[$mode]) {

          $config_values[$entity_type_id][$mode] = TRUE;

          foreach ($entity_type_config[$mode . '_modes'] as $mode_name => $mode_status) {
            if ($mode_status) {
              $config_values[$entity_type_id][$mode . '_modes'][] = $mode_name;
            }
          }

        }
      }
    }
    $this->config(static::SETTINGS)
      ->set('entities', $config_values)
      ->save();

    $this->entityFieldManager->clearCachedFieldDefinitions();

    parent::submitForm($form, $form_state);
  }

}
