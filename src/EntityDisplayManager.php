<?php

namespace Drupal\rest_entity_display;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\rest_entity_display\Exception\EntityDisplayNotActivatedException;
use Drupal\rest_entity_display\Exception\EntityNotFieldableException;
use Drupal\rest_entity_display\Exception\EntityNotFoundException;
use Exception;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Entity display manager class.
 */
class EntityDisplayManager implements EntityDisplayManagerInterface {

  use LoggerChannelTrait;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected ConfigFactoryInterface $configFactory;

  protected LanguageManagerInterface $languageManager;

  protected SerializerInterface $serializer;

  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * Construct a new EntityDisplayManager object.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param LanguageManagerInterface $language_manager
   *   The language manager.
   * @param SerializerInterface $serializer
   *   The serializer.
   * @param EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    LanguageManagerInterface $language_manager,
    SerializerInterface $serializer,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->serializer = $serializer;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function isEntityDisplayActivated(string $entity_type_id, string $mode, string $context): bool {
    $config = $this->configFactory->get('rest_entity_display.settings');
    $entity_config = $config->get('entities.' . $entity_type_id);

    return (
      $entity_config[$context] &&
      in_array($mode, $entity_config[$context . '_modes'])
    );
  }

  /**
   * {@inheritdoc}
   * @throws EntityDisplayNotActivatedException
   * @throws Exception
   */
  public function getNormalizedEntityDisplay(string $entity_type_id, string $entity_id, string $mode, string $context): array {
    try {
      if (!$this->isEntityDisplayActivated($entity_type_id, $mode, $context)) {
        throw new EntityDisplayNotActivatedException('The entity display is not activated in configuration.');
      }
      $entity = $this->getFieldableEntity($entity_type_id, $entity_id);
    }
    catch (Exception $e) {
      $this->getLogger('rest_entity_display')->error('The %entity_type entity display is not activated in configuration for %mode @context mode (entity id: @entity_id).', [
        '%entity_type' => $entity_type_id,
        '%mode' => $mode,
        '@context' => 'view',
        '@entity_id' => $entity_id,
      ]);
      throw $e;
    }

    if ($context == 'form') {
      $entity_display = EntityFormDisplay::collectRenderDisplay($entity, $mode);
    }
    else {
      $entity_display = EntityViewDisplay::collectRenderDisplay($entity, $mode);
    }
    $normalization_context = [
      'entity' => $entity,
      'view_mode' => $mode,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
    ];

    return $this->serializer->normalize($entity_display, 'json', $normalization_context);
  }

  /**
   * {@inheritdoc}
   * @throws PluginNotFoundException
   * @throws InvalidPluginDefinitionException
   * @throws EntityNotFoundException
   * @throws EntityNotFieldableException
   */
  public function getFieldableEntity(string $entity_type_id, string $id): FieldableEntityInterface {
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($id);
    if (!$entity) {
      throw new EntityNotFoundException();
    }
    if (!($entity instanceof FieldableEntityInterface)) {
      throw new EntityNotFieldableException();
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getModeOptions(string $entity_type_id): array {
    return [
      'view' => $this->entityDisplayRepository->getViewModeOptions($entity_type_id),
      'form' => $this->entityDisplayRepository->getFormModeOptions($entity_type_id),
    ];
  }

}
