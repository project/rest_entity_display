<?php

namespace Drupal\rest_entity_display\Normalizer;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\serialization\Normalizer\ConfigEntityNormalizer;

/**
 * JSON normalizer for EntityFormDisplay config entities.
 */
class JsonEntityFormDisplayNormalizer extends ConfigEntityNormalizer {

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array {
    if (!isset($context['entity'])) {
      return [];
    }

    $attributes = [];
    $components = $object->toArray()['content'];
    uasort($components, [SortArray::class, 'sortByWeightElement']);
    foreach ($components as $name => $options) {
      $attributes[$name] = $options;
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL, $context = []): bool {
    return (
      parent::supportsNormalization($data, $format) &&
      $data instanceof EntityFormDisplayInterface
    );
  }

}
