<?php

namespace Drupal\rest_entity_display\Normalizer;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\serialization\Normalizer\ConfigEntityNormalizer;

/**
 * JSON normalizer for EntityViewDisplay config entities.
 */
class JsonEntityViewDisplayNormalizer extends ConfigEntityNormalizer {

  protected LanguageManagerInterface $languageManager;

  protected ModuleHandlerInterface $moduleHandler;

  private ?array $build = NULL;

  /**
   * Construct a JsonEntityViewDisplayNormalizer object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeRepositoryInterface $entity_type_repository,
    EntityFieldManagerInterface $entity_field_manager,
    LanguageManagerInterface $language_manager,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($entity_type_manager, $entity_type_repository, $entity_field_manager);
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Gets the build of an entity after applying hooks.
   *
   * @param $entity
   *   The entity.
   * @param $object
   *   The object.
   * @param $view_mode
   *   The view mode.
   * @return array
   *   The build of the entity.
   */
  private function getBuild($entity, $object, $view_mode): array {
    if ($this->build) {
      return $this->build;
    }

    $build = [];
    $view_hook = "{$entity->getEntityTypeId()}_view";
    $this->moduleHandler->invokeAll($view_hook, [&$build, $entity, $object, $view_mode]);
    $this->moduleHandler->invokeAll('entity_view', [&$build, $entity, $object, $view_mode]);
    $this->moduleHandler->alter([$view_hook, 'entity_view'], $build, $entity, $object);

    $this->build = $build;
    return $build;
  }

  /**
   * {@inheritdoc}
   * @var EntityViewDisplayInterface $object
   */
  public function normalize($object, $format = NULL, array $context = []): array {
    if (!isset($context['entity'])) {
      return [];
    }
    /** @var EntityInterface $entity */
    $entity = $context['entity'];

    $view_mode = $object->getMode();

    $current_language = $context['langcode'] ?? $this->languageManager->getCurrentLanguage()->getId();
    if ($entity instanceof ContentEntityInterface && $entity->hasTranslation($current_language)) {
      $entity = $entity->getTranslation($current_language);
    }

    $attributes = [];
    $components = $object->toArray()['content'];
    uasort($components, [SortArray::class, 'sortByWeightElement']);

    foreach (array_keys($components) as $name) {
      /** @var FormatterBase $renderer */
      if ($renderer = $object->getRenderer($name)) {
        /** @var FieldItemListInterface $items */
        $items = $entity->get($name);
        $items->filterEmptyItems();
        if ($items->access('view')) {
          $renderer->prepareView([$items]);
          $rest_name = $renderer->getSetting('rest_field_name') ?: $name;
          $attributes[$rest_name] = $renderer->viewElements($items, $current_language);
        }
      }
      else {
        $build = $this->getBuild($entity, $object, $view_mode);
        if (array_key_exists($name, $build)) {
          $attributes[$name] = $build[$name];
        }
      }
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL, $context = []): bool {
    return (
      parent::supportsNormalization($data, $format) &&
      $data instanceof EntityViewDisplayInterface
    );
  }

}
