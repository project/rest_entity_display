<?php

namespace Drupal\rest_entity_display;

use Drupal\Core\Form\FormStateInterface;

trait NormalizedFormatterTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        'rest_field_name' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['rest_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REST field name'),
      '#default_value' => $this->getSetting('rest_field_name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    if ($rest_field_name = $this->getSetting('rest_field_name')) {
      $summary[] = $this->t('REST field name: @name', ['@name' => $rest_field_name]);
    }
    return $summary;
  }

}
