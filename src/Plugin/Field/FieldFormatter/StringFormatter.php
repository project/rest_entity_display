<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized String' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_string_formatter",
 *   label = @Translation("Normalized String"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *     "uri",
 *     "uuid",
 *     "email",
 *   },
 * )
 */
class StringFormatter extends NormalizedFieldFormatter {
}
