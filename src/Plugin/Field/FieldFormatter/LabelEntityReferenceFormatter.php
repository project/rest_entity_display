<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\rest_entity_display\NormalizedFormatterTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Plugin implementation of the 'Normalized label entity reference' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_label_entity_reference_formatter",
 *   label = @Translation("Normalized label entity reference"),
 *   field_types = {
 *     "entity_reference_revisions",
 *     "entity_reference",
 *   }
 * )
 */
class LabelEntityReferenceFormatter extends EntityReferenceFormatterBase {

  use SerializerAwareTrait;
  use NormalizedFormatterTrait;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    /** @var SerializerInterface $serializer */
    $serializer = $container->get('serializer');
    $instance->setSerializer($serializer);

    /** @var LanguageManagerInterface $language_manager */
    $language_manager = $container->get('language_manager');
    $instance->setLanguageManager($language_manager);

    return $instance;
  }

  /**
   * Sets the language manager.
   *
   * @param LanguageManagerInterface $language_manager
   *  The language manager.
   */
  public function setLanguageManager(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $current_language = $this->languageManager->getCurrentLanguage()->getId();

    foreach ($items as $delta => $item) {
      /** @var EntityBase $entity */
      $entity = $item->entity;
      if ($entity instanceof ContentEntityInterface && $entity->hasTranslation($current_language)) {
        $entity = $entity->getTranslation($current_language);
      }
      $elements[$delta]['value'] = $entity->label();
    }

    return $elements;
  }

}
