<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\rest_entity_display\NormalizedFormatterTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Abstract formatter plugin for normalized fields.
 */
abstract class NormalizedFieldFormatter extends FormatterBase {

  use SerializerAwareTrait;
  use NormalizedFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    /** @var SerializerInterface $serializer */
    $serializer = $container->get('serializer');
    $instance->setSerializer($serializer);

    return $instance;
  }

  /**
   * {@inheritdoc}
   * @throws ExceptionInterface
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    return $this->serializer->normalize($items, 'json');
  }

}
