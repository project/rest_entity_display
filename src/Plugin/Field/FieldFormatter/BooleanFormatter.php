<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized boolean' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_boolean_formatter",
 *   label = @Translation("Normalized boolean"),
 *   field_types = {
 *     "boolean",
 *   }
 * )
 */
class BooleanFormatter extends NormalizedFieldFormatter {
}
