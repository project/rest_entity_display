<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\LanguageFormatter as LanguageFormatterBase;
use Drupal\rest_entity_display\NormalizedFormatterTrait;

/**
 * Plugin implementation of the 'Normalized Language' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_language_formatter",
 *   label = @Translation("Normalized Language"),
 *   field_types = {
 *     "language",
 *   }
 * )
 */
class LanguageFormatter extends LanguageFormatterBase {

  use NormalizedFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected function viewValue(FieldItemInterface $item): array {
    $languages = $this->getSetting('native_language') ?
      $this->languageManager->getNativeLanguages() :
      $this->languageManager->getLanguages(LanguageInterface::STATE_ALL);
    return [
      'value' => $item->language->getId(),
      'label' => $item->language && isset($languages[$item->language->getId()]) ? $languages[$item->language->getId()]->getName() : '',
    ];
  }

}
