<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized options' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_options_formatter",
 *   label = @Translation("Normalized options"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class OptionsKeyFormatter extends NormalizedFieldFormatter {
}
