<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized long text' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_normalized_text",
 *   label = @Translation("Normalized long text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   },
 * )
 */
class TextFormatter extends NormalizedFieldFormatter {
}
