<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized number' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_number_formatter",
 *   label = @Translation("Normalized number"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   }
 * )
 */
class NumberFormatter extends NormalizedFieldFormatter {
}
