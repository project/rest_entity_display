<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_link_formatter",
 *   label = @Translation("Normalized link text and URL"),
 *   field_types = {
 *     "link",
 *   }
 * )
 */
class LinkFormatter extends NormalizedFieldFormatter {
}
