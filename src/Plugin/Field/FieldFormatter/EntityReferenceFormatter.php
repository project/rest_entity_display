<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized entity reference' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_entity_reference_formatter",
 *   label = @Translation("Normalized entity reference"),
 *   field_types = {
 *     "entity_reference_revisions",
 *     "entity_reference",
 *   }
 * )
 */
class EntityReferenceFormatter extends NormalizedFieldFormatter {
}
