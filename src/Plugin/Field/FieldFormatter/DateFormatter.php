<?php

namespace Drupal\rest_entity_display\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized Date' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_entity_display_date_formatter",
 *   label = @Translation("Normalized Date"),
 *   field_types = {
 *     "datetime",
 *     "timestamp",
 *     "created",
 *     "changed",
 *   }
 * )
 */
class DateFormatter extends NormalizedFieldFormatter {
}
