<?php

namespace Drupal\rest_entity_display\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest_entity_display\EntityDisplayManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Represents EntityViewDisplay records as resources.
 *
 * @RestResource (
 *   id = "rest_entity_display_entity_view_display",
 *   label = @Translation("Entity view display"),
 *   uri_paths = {
 *     "canonical" = "/api/entity_view_display/{entity_type_id}/{identifier}/{view_mode}",
 *   }
 * )
 *
 */
class EntityViewDisplayResource extends ResourceBase {

  protected EntityDisplayManagerInterface $entityDisplayManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $entity_display_manager = $container->get('rest_entity_display.entity_display_manager');
    $instance->setEntityDisplayManager($entity_display_manager);

    return $instance;
  }

  /**
   * Sets the entity display manager.
   *
   * @param EntityDisplayManagerInterface $entity_display_manager
   *   The entity display manager.
   */
  protected function setEntityDisplayManager(EntityDisplayManagerInterface $entity_display_manager) {
    $this->entityDisplayManager = $entity_display_manager;
  }

  /**
   * Responds to GET HTTP method.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $identifier
   *   The entity identifier.
   * @param string $view_mode
   *   The view mode.
   *
   * @return Response
   *   The HTTP response.
   * @throws \Exception
   */
  public function get(string $entity_type_id, string $identifier, string $view_mode): Response {
    try {
      $content = $this->entityDisplayManager->getNormalizedEntityDisplay($entity_type_id, $identifier, $view_mode, 'view');
    }
    catch (Exception $e) {
      throw new NotFoundHttpException('Not found');
    }

    return new JsonResponse($content);
  }

}
